This is a C++ wrapper for the iRobot Roomba Open Interface.

*NOTES
- OpenInterface class is working - it generates well formed commands for the Roomba
- OISensors class is under construction
- No pull requests will be accepted until the deathstar is fully operational...

- To use as is, add "-DDISABLE_SENSORS" to your compile command line
-- Defining DISABLE_SENSORS is not necessary, but it will remove the unusable code from your project.
